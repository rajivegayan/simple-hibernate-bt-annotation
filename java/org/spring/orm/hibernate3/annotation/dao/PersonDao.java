package org.spring.orm.hibernate3.annotation.dao;


import java.util.Collection;

import org.spring.orm.hibernate3.annotation.bean.Person;
import org.springframework.dao.DataAccessException;


public interface PersonDao {

    /**
     * Find persons.
     */
    public Collection<Person> findPersons() throws DataAccessException;
    
    /**
     * Find persons by last name.
     */
    public Collection<Person> findPersonsByLastName(String lastName) throws DataAccessException;


    /**
     * Saves person.
     */
    public void save(Person person);
    
    /**
     * Update a Person
     */
    public void update(Person person);

    /**
     * Delete a Person
     */
    public void delete(Person person);

}
