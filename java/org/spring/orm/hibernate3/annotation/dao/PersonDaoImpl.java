package org.spring.orm.hibernate3.annotation.dao;


import java.util.Collection;

import org.hibernate.SessionFactory;
import org.spring.orm.hibernate3.annotation.bean.Person;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional(readOnly = true)
public class PersonDaoImpl implements PersonDao {

    protected HibernateTemplate template = null;

    /**
     * Sets Hibernate session factory.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        template = new HibernateTemplate(sessionFactory);
    }

    /**
     * Find persons.
     */
    @SuppressWarnings("unchecked")
    public Collection<Person> findPersons() throws DataAccessException {
        return (Collection<Person>) template.find("from Person");
    }

    /**
     * Find persons by last name.
     */
    @SuppressWarnings("unchecked")
    public Collection<Person> findPersonsByLastName(String lastName) throws DataAccessException {
        return (Collection<Person>) template.find("from Person p where p.lastName = ?", lastName);
    }

    /**
     * Saves person.
     */
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void save(Person person) {
        template.save(person);
    }
    
    /**
     * Updates a Person 
     */
    @Transactional(readOnly=false, propagation = Propagation.REQUIRES_NEW)
    public void update(Person person) {
    	template.saveOrUpdate(person);
    }

    /**
     * Deletes a Person 
     */
    @Transactional(readOnly=false, propagation = Propagation.REQUIRES_NEW)
    public void delete(Person person) {
    	template.delete(person);
    }
}
