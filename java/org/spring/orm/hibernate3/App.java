package org.spring.orm.hibernate3;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.spring.orm.hibernate3.annotation.bean.Address;
import org.spring.orm.hibernate3.annotation.bean.Person;
import org.spring.orm.hibernate3.annotation.dao.PersonDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {

    public static void main( String[] args ) {
    
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("Appcontext.xml");
    	PersonDao personDao = (PersonDao) applicationContext.getBean("personDao");
    	
    	// Saving Code goes here
    	/*
    	System.out.println("Creating a Person..");    	    	    
    	Address address1 = new Address();
    	address1.setId(1);
    	address1.setAddress("Level 55");
    	address1.setCity("Adelaide");
    	address1.setState("SA");
    	address1.setZipPostal("5000");
    	address1.setCreated(new Date());
    	
    	Address address2 = new Address();
    	address2.setId(2);
    	address2.setAddress("16");
    	address2.setCity("Adelaide");
    	address2.setState("SA");
    	address2.setZipPostal("5001");
    	address2.setCreated(new Date());
    	
    	Set<Address> addresses = new HashSet<Address>();
    	addresses.add(address1);
    	addresses.add(address2);
    	
    	Person person1 = new Person();
    	person1.setId(1);
    	person1.setFirstName("Glen");
    	person1.setLastName("Hawson");
    	person1.setCreated(new Date());
    	person1.setAddresses(addresses);
    	
    	personDao.save(person1);
    	System.out.println("End Of Creating a Person");
    	*/
    	
    	//Updating
    	/*
    	System.out.println("Updating a Person..");    	    	    
    	Address address1Up = new Address();
    	address1Up.setId(1);
    	address1Up.setAddress("Level 66");
    	address1Up.setCity("Adelaide");
    	address1Up.setState("SA");
    	address1Up.setZipPostal("5000");
    	address1Up.setCreated(new Date());
    	
    	Address address2Up = new Address();
    	address2Up.setId(2);
    	address2Up.setAddress("44444");
    	address2Up.setCity("Adelaide");
    	address2Up.setState("SA");
    	address2Up.setZipPostal("5001");
    	address2Up.setCreated(new Date());
    	
    	Set<Address> addressesUp = new HashSet<Address>();
    	addressesUp.add(address1Up);
    	addressesUp.add(address2Up);
    	
    	Person person1Up = new Person();
    	person1Up.setId(1);
    	person1Up.setFirstName("Mark");
    	person1Up.setLastName("Anthony");
    	person1Up.setCreated(new Date());
    	person1Up.setAddresses(addressesUp);
    	
    	personDao.update(person1Up);
    	System.out.println("End Of Updating a Person");
    	*/
    	
    	// Retrive the persons
    	/*
    	Collection<Person> retrivedPersons = personDao.findPersons();
    	System.out.println("Retrived Persons  = "+retrivedPersons.size());
    	
    	Set<Address> reAddress = null;
    	for (Person personSS : retrivedPersons) {
			System.out.println("Person Id = "+personSS.getId());
			System.out.println("Person First Name = "+personSS.getFirstName());
			System.out.println("Person Last Name = "+personSS.getLastName());
			System.out.println("Person Created = "+personSS.getCreated());
			System.out.println(" \n \t\t Address List\n\n");
			reAddress = personSS.getAddresses();
			
			for (Address address : reAddress) {
				
				System.out.println(address.getId()+" - "+address.getAddress()+" - "+address.getCity()
						+" - "+address.getState()+" - "+address.getZipPostal()+" - "+address.getCreated());
			}
		}
		*/
    	
    	//Delete the Persons
    	/*
    	System.out.println("Starting to delete...");
    	Person delPerson = new Person();
    	delPerson.setId(1);
    	delPerson.setFirstName("Mark");
    	delPerson.setLastName("Anthony");
    	
    	personDao.delete(delPerson);
    	System.out.println("Completed the deleting...");
    	*/
    }
}
